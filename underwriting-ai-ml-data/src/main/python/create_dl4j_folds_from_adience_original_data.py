import os
from shutil import copyfile

images_dir = "../resources/adience/data/aligned"
original_folds_dir = "../resources/adience/folds/train_val_txt_files_per_fold"
out_files_dir = "../resources/training/data"

age_list = ["(0, 2)", "(4, 6)", "(8, 12)", "(15, 20)", "(25, 32)", "(38, 43)", "(48, 53)", "(60, 100)"]
gender_list = ["m", "f"]


def organize(source_file_name):
    for fold_idx in range(5):
        fold_out_folder_name_age = "age_training_fold_{0}".format(fold_idx)
        fold_out_folder_name_age = os.path.join(out_files_dir, fold_out_folder_name_age)
        fold_out_folder_name_gender = "gender_training_fold_{0}".format(fold_idx)
        fold_out_folder_name_gender = os.path.join(out_files_dir, fold_out_folder_name_gender)

        if not os.path.exists(fold_out_folder_name_age):
            os.mkdir(fold_out_folder_name_age)
        if not os.path.exists(fold_out_folder_name_gender):
            os.mkdir(fold_out_folder_name_gender)

        # test files
        fold_in_dir = "test_fold_is_{0}".format(fold_idx)
        fold_in_filename = "{0}.txt".format(source_file_name)
        fold_in_filename = os.path.join(original_folds_dir, fold_in_dir, fold_in_filename)

        with open(fold_in_filename) as f:
            def_lines = f.readlines()

        def_lines.pop(0)

        for def_line in def_lines:
            subject_dir = def_line.split("\t")[0]
            image_subject = def_line.split("\t")[2]

            image_name = "landmark_aligned_face.{0}.{1}".format(image_subject, def_line.split("\t")[1])

            image_age = def_line.split("\t")[3]
            # note that since there"s a bug in the txt files - (25 32) == (25 23)
            if image_age == "(25, 23)":
                image_age = "(25, 32)"

            image_gender = def_line.split("\t")[4]

            # copy image file to the associated label (age and gender) directories
            src_image_filename = "{0}/{1}".format(subject_dir, image_name)
            src_image_filename = os.path.join(images_dir, src_image_filename)
            dst_image_filename = "{0}_{1}".format(subject_dir, image_name)

            if image_age in age_list:
                image_age_index = age_list.index(image_age)

                age_label_dir = os.path.join(fold_out_folder_name_age, "{0}".format(image_age_index))
                if not os.path.exists(age_label_dir):
                    os.mkdir(age_label_dir)

                dst_image_filename_age = os.path.join(age_label_dir, dst_image_filename)
                copyfile(src_image_filename, dst_image_filename_age)

            if image_gender in gender_list:
                image_gender_index = gender_list.index(image_gender)

                gender_label_dir = os.path.join(fold_out_folder_name_gender, "{0}".format(image_gender_index))
                if not os.path.exists(gender_label_dir):
                    os.mkdir(gender_label_dir)

                dst_image_filename_gender = os.path.join(gender_label_dir, dst_image_filename)
                copyfile(src_image_filename, dst_image_filename_gender)
    return
