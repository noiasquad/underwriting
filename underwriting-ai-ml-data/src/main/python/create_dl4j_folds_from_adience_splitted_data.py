import os
from shutil import copyfile

images_path = "../resources/adience/data/aligned"
folds_input_path = "../resources/adience/folds/train_val_txt_files_per_fold"
folds_output_path = "../resources/training/data"

age_list = ["(0, 2)", "(4, 6)", "(8, 12)", "(15, 20)", "(25, 32)", "(38, 43)", "(48, 53)", "(60, 100)"]
gender_list = ["m", "f"]


def organize(fold_name, dataset_name):
    for fold_idx in range(5):
        # input dataSet file
        fold_input_file_path = os.path.join(
            folds_input_path, "{0}_{1}".format(fold_name, fold_idx), "{0}.txt".format(dataset_name))

        # output dir
        fold_output_dir_path = os.path.join(folds_output_path, "{0}_{1}".format(dataset_name, fold_idx))

        if not os.path.exists(fold_output_dir_path):
            os.mkdir(fold_output_dir_path)

        with open(fold_input_file_path) as f:
            observations = f.readlines()

        for observation in observations:
            # tokenize observation line
            split = observation.split(" ")

            # extract feature image file path from observation line
            feature_image_file_path = split[0]
            # extract feature image file name from feature image file path
            feature_image_file_name = feature_image_file_path.split("/")[1]
            # extract label from observation line
            label = split[1].split("\n")[0]

            # prepare the dedicated label directory
            label_dir = os.path.join(fold_output_dir_path, "{0}".format(label))
            if not os.path.exists(label_dir):
                os.mkdir(label_dir)

            # copy the source image file to the dedicated label directory
            copyfile(os.path.join(images_path, feature_image_file_path),
                     os.path.join(label_dir, feature_image_file_name))
    return


organize("test_fold_is", "age_test")
organize("test_fold_is", "age_train")
organize("test_fold_is", "age_train_subset")
organize("test_fold_is", "age_val")
organize("test_fold_is", "gender_test")
organize("test_fold_is", "gender_train")
organize("test_fold_is", "gender_train_subset")
organize("test_fold_is", "gender_val")
