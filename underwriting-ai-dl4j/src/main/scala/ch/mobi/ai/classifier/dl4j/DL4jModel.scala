package ch.mobi.ai.classifier.dl4j

import java.nio.file._

import ch.mobi.ai.domain.{Model, Prediction}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.util.ModelSerializer
import org.nd4j.linalg.api.ndarray.INDArray

import scala.util.{Failure, Success, Try}

case class DL4jModel(val network: MultiLayerNetwork) extends Model[INDArray, Int] {

  override def model: INDArray => Try[Array[Prediction[Int]]] =
    vectorizedImage =>
      Success(network
        .output(vectorizedImage)
        .toDoubleVector
        .zipWithIndex.map { case (confidence, index) => Prediction(index, confidence) })

  override def init: Unit = {
    network.init()
  }
}

object DL4jModel {

  def create(path: Path): Try[DL4jModel] =
    if (!path.toFile.exists()) Failure(new RuntimeException(s"Model file doest not exists: ${path.getFileName}"))
    else Success(DL4jModel(ModelSerializer.restoreMultiLayerNetwork(path.toFile)))
}
