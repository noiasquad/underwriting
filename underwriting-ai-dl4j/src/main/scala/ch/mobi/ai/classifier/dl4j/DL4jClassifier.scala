package ch.mobi.ai.classifier.dl4j

import java.nio.file.Path

import ch.mobi.ai.domain.{Classifier, Prediction}
import javax.imageio.ImageIO
import org.datavec.image.loader.NativeImageLoader
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler

import scala.util.{Success, Try}

case class DL4jClassifier(network: DL4jModel, width: Int, height: Int, depth: Int) extends Classifier[Path, Int] {

  private val loader = new NativeImageLoader(width, height, depth, true)
  private val scaler = new ImagePreProcessingScaler(0, 1)

  override def classify(faceImage: Path): Try[Array[Prediction[Int]]] =
    Success(ImageIO.read(faceImage.toFile))
      .map(i => loader.asMatrix(i))
      .map(v => scale(v))
      .flatMap(network.model(_))

  private def scale(vectorizedImage: INDArray): INDArray = {
    scaler.transform(vectorizedImage)
    vectorizedImage
  }
}

object DL4jClassifier {
  def gpu(model: DL4jModel, width: Int, height: Int, depth: Int): DL4jClassifier =
    DL4jClassifier(model, width, height, depth)
}