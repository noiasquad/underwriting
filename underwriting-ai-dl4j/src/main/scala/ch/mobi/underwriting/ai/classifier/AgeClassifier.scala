package ch.mobi.underwriting.ai.classifier

import java.nio.file.Path

import ch.mobi.ai.classifier.dl4j.{DL4jClassifier, DL4jModel}
import ch.mobi.ai.domain.{Classifier, Prediction}

import scala.util.{Failure, Success, Try}

class AgeClassifier(val rootPath: Path, width: Int = 227, height: Int = 227, depth: Int = 3, val gpuMode: Boolean = false) extends Classifier[Path, Int] {

  private val classifier = model(rootPath) match {
    case Success(model) =>
      if (gpuMode) DL4jClassifier(model, width, height, depth)
      else DL4jClassifier.gpu(model, width, height, depth)
    case Failure(e) => throw e
  }

  override def classify(faceImage: Path): Try[Array[Prediction[Int]]] = classifier.classify(faceImage)

  private def model(rootPath: Path): Try[DL4jModel] = {
    val defaultAgeTrainedModelPath = "age/age_net.dl4j"
    DL4jModel.create(rootPath.resolve(defaultAgeTrainedModelPath))
  }
}
