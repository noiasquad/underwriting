package ch.mobi.underwriting.selfie.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SelfieApplication {

    public static void main(String[] args) {
        SpringApplication.run(SelfieApplication.class, args);
    }
}
