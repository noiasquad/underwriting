import {Component} from '@angular/core';
import {WebcamImage, WebcamInitError} from 'ngx-webcam';
import {Subject} from 'rxjs/Subject';
import {Observable} from 'rxjs/Observable';
import {ProfilerService} from '../../core/service/profiler.service';
import {NotifierService} from '../../core/service/notifier.service';
import {Prediction} from '../../core/model/profile.model';
import {ProfilerClient} from '../../core/service/client/profiler-client.service';

@Component({
  selector: 'app-selfie-capture',
  templateUrl: './selfie-capture.component.html',
  styleUrls: ['./selfie-capture.component.css']
})
export class SelfieCaptureComponent {

  webcamActivated = false;
  lastSnapshot: WebcamImage = null;
  private snapshotRequests: Subject<void> = new Subject<void>();

  constructor(private client: ProfilerClient,
              private service: ProfilerService,
              private notifier: NotifierService) {
  }

  public get snapshotRequestObservable(): Observable<void> {
    return this.snapshotRequests.asObservable();
  }

  public doSnapshot(): void {
    this.snapshotRequests.next();
  }

  public toggleWebcam(): void {
    this.webcamActivated = !this.webcamActivated;
  }

  public handleCapture(image: WebcamImage): void {
    this.lastSnapshot = image;
    this.toggleWebcam();
  }

  public handleWebcamError(error: WebcamInitError): void {
    this.notifier.failure(error.message, 'capture');
  }

  public validateCapture() {
    this.client.upload$(this.lastSnapshot.imageAsBase64).subscribe(
      data => {
        this.service.profiles.next(<Prediction>{...data});
        this.notifier.success('The selfie has been successfully uploaded', 'capture');
      },
      error => {
        this.notifier.success('An error ocurred when uploading the selfie', 'capture');
      }
    );
    this.lastSnapshot = undefined;
  }

  public continueCapture() {
    this.lastSnapshot = undefined;
    this.toggleWebcam();
  }
}
