import {Component, OnDestroy, OnInit} from '@angular/core';
import {ProfilerService} from '../../core/service/profiler.service';
import {Subscription} from 'rxjs/Subscription';
import {NotifierService} from '../../core/service/notifier.service';
import {Prediction, Profile} from "../../core/model/profile.model";

type Mode = 'ESTIMATING' | 'PRESENTING' | 'CORRECTING';

@Component({
  selector: 'app-profile-presenter',
  templateUrl: './profile-presenter.component.html',
  styleUrls: ['./profile-presenter.component.css']
})
export class ProfilePresenterComponent implements OnInit, OnDestroy {

  prediction: Prediction;
  correction: Profile;
  mode: Mode;

  private requestSubscription: Subscription;
  private profileRubscription: Subscription;

  constructor(private service: ProfilerService,
              private notifier: NotifierService) {
    this.prediction = this.correction = undefined;
    this.mode = undefined;
  }

  ngOnInit() {
    this.requestSubscription = this.service.requests.subscribe(r => {
      this.prediction = undefined;
      this.mode = 'ESTIMATING';
    });
    this.profileRubscription = this.service.profiles.subscribe(p => {
      this.prediction = Prediction.of(p);
      this.mode = 'PRESENTING';
    });
  }

  ngOnDestroy(): void {
    this.requestSubscription.unsubscribe();
    this.profileRubscription.unsubscribe();
  }

  get profile(): Profile {
    return this.correction ? this.correction : this.prediction;
  }

  get genderProbabilities(): number[] {
    return this.prediction.genderProbabilities;
  }

  get ageProbabilities(): number[] {
    return this.prediction.ageProbabilities;
  }

  get weightProbabilities(): number[] {
    return this.prediction.weightProbabilities;
  }

  validateEstimate() {
    this.notifier.success('Thank you for helping in improving this profiler.','presenter');
  }

  startCorrection() {
    this.mode = 'CORRECTING';
    this.correction = undefined;
  }

  onCorrectionSubmitted(correction: Profile) {
    this.notifier.success('Thank you for helping in improving this profiler.', 'corrector');
    this.mode = 'PRESENTING';
    this.correction = <Profile>{...correction};
  }

  onCorrectionAbandoned() {
    this.mode = 'PRESENTING';
    this.correction = undefined;
  }

  get isCorrecting(): boolean {
    return this.mode === 'CORRECTING'
  }

  get isPresenting(): boolean {
    return this.mode === 'PRESENTING'
  }

  get isEstimating(): boolean {
    return this.mode === 'ESTIMATING'
  }

  get genderHasBeenCorrected(): boolean {
    return this.prediction && this.correction && this.prediction.gender != this.correction.gender;
  }

  get ageHasBeenCorrected(): boolean {
    return this.prediction && this.correction && this.prediction.age != this.correction.age;
  }

  get weightHasBeenCorrected(): boolean {
    return this.prediction && this.correction && this.prediction.weight != this.correction.weight;
  }
}
