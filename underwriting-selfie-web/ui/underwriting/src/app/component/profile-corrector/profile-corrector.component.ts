import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Prediction, Profile} from '../../core/model/profile.model';
import {ProfilerClient} from '../../core/service/client/profiler-client.service';
import {NotifierService} from '../../core/service/notifier.service';

@Component({
  selector: 'app-profile-corrector',
  templateUrl: './profile-corrector.component.html',
  styleUrls: ['./profile-corrector.component.css']
})
export class ProfileCorrectorComponent implements OnInit {

  @Input() profile: Prediction;
  @Output() onCorrectionSubmitted: EventEmitter<Profile>;
  @Output() onCorrectionAbandoned: EventEmitter<any>;
  fg: FormGroup;

  constructor(private fb: FormBuilder,
              private client: ProfilerClient,
              private notifier: NotifierService) {
    this.onCorrectionSubmitted = new EventEmitter<Profile>();
    this.onCorrectionAbandoned = new EventEmitter<any>();
  }

  ngOnInit() {
    this.fg = this.fb.group({
      gender: [this.profile.gender, Validators.required],
      age: [this.profile.age, Validators.required],
      weight: [this.profile.weight, Validators.required]
    });
  }

  submitCorrection() {
    const correction: Profile = <Profile>{...this.fg.value};
    this.client.train$(this.profile.id, correction).subscribe(
      response => this.onCorrectionSubmitted.emit(correction),
      error => this.notifier.failure('An error occurred when submitting the correction', 'corrector'));
  }

  abandonCorrection() {
    this.onCorrectionAbandoned.emit();
  }

}
