import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileCorrectorComponent } from './profile-corrector.component';

describe('ProfileCorrectorComponent', () => {
  let component: ProfileCorrectorComponent;
  let fixture: ComponentFixture<ProfileCorrectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileCorrectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileCorrectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
