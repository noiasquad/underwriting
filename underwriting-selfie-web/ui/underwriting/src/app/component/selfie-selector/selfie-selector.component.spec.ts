import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SelfieSelectorComponent } from './selfie-selector.component';

describe('SelfieSelectorComponent', () => {
  let component: SelfieSelectorComponent;
  let fixture: ComponentFixture<SelfieSelectorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelfieSelectorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SelfieSelectorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
