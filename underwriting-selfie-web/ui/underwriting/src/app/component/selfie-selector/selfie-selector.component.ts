import {Component} from '@angular/core';
import {ProfilerClient} from '../../core/service/client/profiler-client.service';
import {Prediction} from '../../core/model/profile.model';
import {ProfilerService} from '../../core/service/profiler.service';
import {Request} from '../../core/model/request.model';
import {NotifierService} from '../../core/service/notifier.service';

@Component({
  selector: 'app-selfie-selector',
  templateUrl: './selfie-selector.component.html',
  styleUrls: ['./selfie-selector.component.css']
})
export class SelfieSelectorComponent {

  constructor(private service: ProfilerService,
              private client: ProfilerClient,
              private notifier: NotifierService) {
  }

  get profilerUrl(): string {
    return this.client.predictionUrl;
  }

  get fileSizeLimit(): number {
    return this.client.fileSizeLimit;
  }

  get imageFormatFilter(): string {
    return this.client.imageFormatFilter;
  }

  onBeforeUpload() {
    this.service.requests.next(new Request());
  }

  onUpload(event) {
    this.service.profiles.next(<Prediction>{...JSON.parse(event.xhr.response)});
    this.notifier.success('The selfie has been successfully uploaded.', 'upload');
  }
}
