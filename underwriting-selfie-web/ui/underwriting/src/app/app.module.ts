import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {FileUploadModule} from 'primeng/fileupload';
import {GrowlModule} from 'primeng/growl';
import {ButtonModule} from 'primeng/button';
import {CardModule} from 'primeng/card';
import {MessageService} from 'primeng/components/common/messageservice';
import {DropdownModule, RadioButtonModule, SpinnerModule} from 'primeng/primeng';
import {SliderModule} from 'primeng/slider';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {ProgressBarModule} from 'primeng/progressbar';
import {AccordionModule} from 'primeng/accordion';
import {WebcamModule} from 'ngx-webcam';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';

import {AppComponent} from './app.component';
import {SelfieSelectorComponent} from './component/selfie-selector/selfie-selector.component';
import {ProfilePresenterComponent} from './component/profile-presenter/profile-presenter.component';
import {ProfileCorrectorComponent} from './component/profile-corrector/profile-corrector.component';
import {ProfilerService} from './core/service/profiler.service';
import {ProfilerClient} from './core/service/client/profiler-client.service';
import {NotifierService} from './core/service/notifier.service';
import {SelfieCaptureComponent} from './component/selfie-capture/selfie-capture.component';
import {TranslateHttpLoader} from "@ngx-translate/http-loader";

@NgModule({
  declarations: [
    AppComponent,
    SelfieSelectorComponent,
    ProfilePresenterComponent,
    ProfileCorrectorComponent,
    SelfieCaptureComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule,
    GrowlModule,
    ButtonModule,
    CardModule,
    SpinnerModule,
    RadioButtonModule,
    HttpClientModule,
    DropdownModule,
    SliderModule,
    ProgressSpinnerModule,
    ProgressBarModule,
    AccordionModule,
    BrowserAnimationsModule,
    WebcamModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    ProfilerService,
    ProfilerClient,
    NotifierService,
    MessageService,
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule {
}

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
