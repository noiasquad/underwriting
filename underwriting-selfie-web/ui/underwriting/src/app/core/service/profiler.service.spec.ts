import { TestBed, inject } from '@angular/core/testing';

import { ProfilerService } from './profiler.service';

describe('ProfilerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfilerService]
    });
  });

  it('should be created', inject([ProfilerService], (service: ProfilerService) => {
    expect(service).toBeTruthy();
  }));
});
