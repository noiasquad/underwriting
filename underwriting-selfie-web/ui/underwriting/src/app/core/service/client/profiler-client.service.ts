import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';

import {Prediction, Profile} from '../../model/profile.model';
import {fromB64} from "../../util/blob-factory";

@Injectable()
export class ProfilerClient {

  constructor(private http: HttpClient) {
  }

  get url(): string {
//    return 'http://localhost:9000/api/profiling/v1/predictions';
    return 'https://profiling.obya.ch:9100/api/profiling/v1/predictions';
  }

  get predictionUrl(): string {
    return `${this.url}/all`;
  }

  get fileSizeLimit(): number {
    return 1000000;
  }

  get imageFormatFilter(): string {
    return 'image/*';
  }

  upload$(imageBase64: string): Observable<Prediction> {
    let formData: FormData = new FormData();
    formData.append('data', fromB64(imageBase64, 'image/jpg'), 'snapshot.jpg');
    const headers = new HttpHeaders({'enctype': 'multipart/form-data'});
    return this.http.post<Prediction>(this.predictionUrl, formData, {headers: headers});
  }

  train$(profileId: string, profile: Profile): Observable<Profile> {
    return this.http.put<Profile>(`${this.url}/${profileId}/correction`, profile);
  }
}
