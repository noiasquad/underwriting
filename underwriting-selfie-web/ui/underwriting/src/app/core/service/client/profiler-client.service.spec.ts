import { TestBed, inject } from '@angular/core/testing';

import { ProfilerClientService } from './profiler-client.service';

describe('ProfilerClientService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfilerClientService]
    });
  });

  it('should be created', inject([ProfilerClientService], (service: ProfilerClientService) => {
    expect(service).toBeTruthy();
  }));
});
