import {Injectable} from '@angular/core';
import {Prediction} from '../model/profile.model';
import {Request} from '../model/request.model';
import {Subject} from 'rxjs/Subject';
import {ProfilerClient} from './client/profiler-client.service';
import {NotifierService} from './notifier.service';

@Injectable()
export class ProfilerService {

  profiles: Subject<Prediction> = new Subject();
  requests: Subject<Request> = new Subject();

  constructor(private client: ProfilerClient,
              private notifier: NotifierService) {
  }
}
