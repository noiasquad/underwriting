export class Profile {
  constructor(
    public readonly gender: number,
    public readonly age: number,
    public readonly weight: number) {
  }
}

const max = (maxValueIndex, currentValue, currentIndex, probabilities) =>
  currentValue > probabilities[maxValueIndex] ? currentIndex : maxValueIndex;

export class Prediction extends Profile {

  static of(prediction: Prediction): Prediction {
    return new Prediction(prediction.content, prediction.id)
  }

  constructor(
    public content: number[][],
    public id?: string) {

    super(
      content[0].reduce(max, 0),
      content[1].reduce(max, 0),
      content[2].reduce(max, 0))
  }

  get genderProbabilities() {
    return this.content[0]
  }

  get ageProbabilities() {
    return this.content[1]
  }

  get weightProbabilities() {
    return this.content[2]
  }
}
