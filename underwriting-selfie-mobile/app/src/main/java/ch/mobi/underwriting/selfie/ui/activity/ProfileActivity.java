package ch.mobi.underwriting.selfie.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.numberprogressbar.NumberProgressBar;
import com.sentaca.android.accordion.utils.FontUtils;

import java.util.UUID;

import ch.mobi.underwriting.selfie.ui.R;
import ch.mobi.underwriting.selfie.ui.data.model.Prediction;
import ch.mobi.underwriting.selfie.ui.data.model.Profile;

public class ProfileActivity extends AppCompatActivity {

    private static final String BUNDLE_PREDICTION_ID = "prediction-id";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        final Prediction prediction = (Prediction)intent.getSerializableExtra(BUNDLE_PREDICTION_ID);
        Profile profile = prediction.getProfile();

        setContentView(R.layout.activity_profile);
        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        LinearLayout gender = findViewById(R.id.content_profile_gender);
        LinearLayout age = findViewById(R.id.content_profile_age);
        LinearLayout weight = findViewById(R.id.content_profile_weight);

        showPrediction(gender, R.array.gender_labels, prediction.getContent()[0], profile.getGender());
        showPrediction(age, R.array.age_labels, prediction.getContent()[1], profile.getAge());
        showPrediction(weight, R.array.weight_labels, prediction.getContent()[2], profile.getWeight());
/*
        FloatingActionButton fab = findViewById(R.id.fab_correction);
        fab.setOnClickListener(view -> startCorrection(prediction));*/
    }

    private void showPrediction(LinearLayout predictionLayout,
                                int labelNamesResId,
                                double[] probabilities,
                                int prediction) {

        String[] labelNames = getResources().getStringArray(labelNamesResId);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);

        int classIdx = 0;
        for (double probability: probabilities) {
            TextView labelText = new TextView(this);
            labelText.setText(labelNames[classIdx]);
            labelText.setTextSize(18);
            FontUtils.setCustomFont(labelText, getAssets());
            predictionLayout.addView(labelText, layoutParams);

            NumberProgressBar probabilityBar = new NumberProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
            probabilityBar.setProgressTextSize(28);
            probabilityBar.setProgress(asPercentage(probability));
            predictionLayout.addView(probabilityBar, layoutParams);

            if (classIdx == prediction) {
                labelText.setTextSize(20);
                labelText.setTypeface(labelText.getTypeface(), Typeface.BOLD);
                probabilityBar.setProgressTextSize(36);
            }

            classIdx++;
        }
    }

    private int asPercentage(double probability) {
        return (int)(probability * 100.0);
    }

    public static Intent makeIntent(Context context, Prediction prediction) {
        Intent intent = new Intent(context, ProfileActivity.class);
        intent.putExtra(BUNDLE_PREDICTION_ID, prediction);
        return intent;
    }

    private void startCorrection(Prediction prediction) {
        startActivity(CorrectionActivity.makeIntent(getApplicationContext(), prediction));
    }
}