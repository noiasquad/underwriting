package ch.mobi.underwriting.selfie.ui.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import ch.mobi.underwriting.selfie.ui.R;
import ch.mobi.underwriting.selfie.ui.data.model.Prediction;
import ch.mobi.underwriting.selfie.ui.service.PhotoUploadService;
import ch.mobi.underwriting.selfie.ui.task.RemoteServerLivenessCheck;

public class CaptureActivity extends AppCompatActivity {

    private final int REQUEST_CAPTURE_IMAGE = 100;

    private PhotoUploadResultReceiver photoUploadResultReceiver;

    private boolean livenessChecked = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        registerReceiver();

        setContentView(R.layout.activity_capture);
        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab_capture);
        fab.setOnClickListener(view -> openCameraIntent());

        super.onCreate(savedInstanceState);

        checkLiveness();
    }

    private void checkLiveness() {
        if (!livenessChecked) {
            livenessChecked = !livenessChecked;
            new RemoteServerLivenessCheck(getApplicationContext()).execute();
        }
    }

    private void registerReceiver() {
        photoUploadResultReceiver = new PhotoUploadResultReceiver();
        IntentFilter intentFilter = new IntentFilter(PhotoUploadService.ACTION_UPLOAD_SERVICE_RESPONSE);
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(photoUploadResultReceiver, intentFilter);
    }

    public class PhotoUploadResultReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Prediction prediction = (Prediction)intent.getSerializableExtra("prediction");
            startActivity(ProfileActivity.makeIntent(context, prediction));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_capture, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openCameraIntent() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (pictureIntent.resolveActivity(getPackageManager()) != null){
            startActivityForResult(pictureIntent, REQUEST_CAPTURE_IMAGE);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(photoUploadResultReceiver);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CAPTURE_IMAGE && resultCode == RESULT_OK) {
            if (data != null && data.hasExtra("data")) {
                this.startService(PhotoUploadService.makeIntent(getApplicationContext(), (Bitmap) data.getExtras().get("data")));
            }
        }
        else if(resultCode == RESULT_CANCELED) {
            // User Cancelled the action
        }
    }
}
