package ch.mobi.underwriting.selfie.ui.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import ch.mobi.underwriting.selfie.ui.R;
import ch.mobi.underwriting.selfie.ui.data.model.Prediction;
import ch.mobi.underwriting.selfie.ui.data.provider.MediaFileExternalStorage;
import ch.mobi.underwriting.selfie.ui.data.retrofit.PredictionService;
import ch.mobi.underwriting.selfie.ui.data.retrofit.ProxyFactory;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

/**
 * Intent Service that will run in background. It stores and uploads the photos for prediction.
 */
public class PhotoUploadService extends IntentService {
    private final String TAG = getClass().getSimpleName();

    public static final String ACTION_UPLOAD_SERVICE_RESPONSE = "UploadPhotoService.RESPONSE";

    private static final int NOTIFICATION_ID = 1;

    private PredictionService predictionService;

    private MediaFileExternalStorage mediaFileExternalStorage;

    private NotificationManager notifier;

    private Builder notificationBuilder;

    public PhotoUploadService() {
        super("UploadPhotoService");
        this.predictionService = ProxyFactory.create(PredictionService.class);
        this.mediaFileExternalStorage = new MediaFileExternalStorage();
    }

    public static Intent makeIntent(Context context, Bitmap bitmap) {
        return new Intent(context, PhotoUploadService.class).putExtra("data", bitmap);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bitmap photoBitmap = (Bitmap)intent.getExtras().get("data");

        File photoFile = registerPhoto(photoBitmap);
        if (photoFile != null) {
            startNotification();

            Prediction prediction = uploadPhoto(photoFile);
            if (prediction != null) {
                finishNotification("The selfie has been successfully uploaded");
            } else {
                finishNotification("The selfie could not be uploaded");
            }
            sendBroadcast(prediction);
        } else {
            finishNotification("The selfie could not be written on the device");
        }
    }

    private File registerPhoto(Bitmap photoBitmap) {
        try {
            if (mediaFileExternalStorage.isReady()) {
                File photoFile = createImageFile(getApplicationContext());
                mediaFileExternalStorage.writeImage(photoFile, photoBitmap);
                return photoFile;
            } else {
                Log.i(TAG, "The media storage is not ready");
                Toast.makeText(getApplicationContext(), "The media storage is not ready", Toast.LENGTH_LONG).show();
            }
        } catch (IOException e) {
            Log.i(TAG, "The photo could not be written into the media storage", e);
            Toast.makeText(getApplicationContext(), "The photo could not be written into the media storage: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return null;
    }

    private Prediction uploadPhoto(File photoFile) {
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), photoFile);
        MultipartBody.Part data = MultipartBody.Part.createFormData(PredictionService.DATA_PARAMETER, photoFile.getName(), requestFile);
        try {
            Response<Prediction> response = predictionService.upload(data).execute();
            Prediction prediction = response.body();
            return prediction;
        } catch (IOException e) {
            Log.i(TAG, "An error occurred when querying the prediction service", e);
            Toast.makeText(getApplicationContext(), "An error occurred when querying the prediction service: " + e.getMessage(), Toast.LENGTH_LONG).show();
        }
        return null;
    }

    private void sendBroadcast(Prediction prediction) {
        sendBroadcast(new Intent(ACTION_UPLOAD_SERVICE_RESPONSE)
                .addCategory(Intent.CATEGORY_DEFAULT)
                .putExtra("prediction", prediction));
    }

    private void startNotification() {
        notifier = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationBuilder = new NotificationCompat.Builder(this).setContentTitle("Selfie upload").setContentText("Upload in progress").setSmallIcon(R.drawable.ic_notify_file_upload).setProgress(0, 0, true);
        notifier.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    private void finishNotification(String status) {
        notificationBuilder.setContentText(status);
        notificationBuilder.setProgress(0, 0, false);
        notifier.notify(NOTIFICATION_ID, notificationBuilder.build());
    }

    private File createImageFile(Context context) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "snapshot_" + timeStamp + "_";
        File storageDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }
}
