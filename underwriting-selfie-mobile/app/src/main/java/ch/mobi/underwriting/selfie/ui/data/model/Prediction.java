package ch.mobi.underwriting.selfie.ui.data.model;

import java.io.Serializable;
import java.util.UUID;

public class Prediction implements Serializable {
    private UUID id;
    private double[][] content;

    public void setId(UUID id) {
        this.id = id;
    }

    public void setContent(double[][] content) {
        this.content = content;
    }

    public UUID getId() {
        return id;
    }

    public double[][] getContent() {
        return content;
    }

    public Profile getProfile() {
        return new Profile(max(content[0]), max(content[1]), max(content[2]));
    }

    private static int max(double[] probabilities) {
        int max = 0;
        int curr = 0;
        double maxProbability = 0.0;
        for (double probability: probabilities) {
            if (probability > maxProbability) {
                max = curr;
                maxProbability = probability;
            }
            curr++;
        }
        return max;
    }
}
