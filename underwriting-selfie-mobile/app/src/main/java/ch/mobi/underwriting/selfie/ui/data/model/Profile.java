package ch.mobi.underwriting.selfie.ui.data.model;

import java.io.Serializable;
import java.util.UUID;

public class Profile implements Serializable {
    private int gender;
    private int age;
    private int weight;

    public Profile(int gender, int age, int weight) {
        this.gender = gender;
        this.age = age;
        this.weight = weight;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Profile{" + "gender=" + gender + ", age=" + age + ", weight=" + weight + '}';
    }
}
