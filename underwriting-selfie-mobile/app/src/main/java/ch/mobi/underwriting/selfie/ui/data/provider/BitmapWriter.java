package ch.mobi.underwriting.selfie.ui.data.provider;

import android.graphics.Bitmap;

import java.io.OutputStream;

public class BitmapWriter {
	public static void write(Bitmap bitmap, OutputStream ostream) {
		bitmap.compress(Bitmap.CompressFormat.JPEG, 100, ostream);
	}
}
