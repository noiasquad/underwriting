package ch.mobi.underwriting.selfie.ui.data.provider;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class MediaFileExternalStorage {

	public boolean isReady() {
		return isExternalStorageReadable() && isExternalStorageWritable();
	}
	
	private boolean isExternalStorageWritable() {
	    return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
	}

	private boolean isExternalStorageReadable() {
	    String state = Environment.getExternalStorageState();
	    return (Environment.MEDIA_MOUNTED.equals(state) || Environment.MEDIA_MOUNTED_READ_ONLY.equals(state));
	}

	public void writeImage(File file, Bitmap image) throws IOException {
		OutputStream os = new FileOutputStream(file);
		BitmapWriter.write(image, os);
		os.close();
	}
}
