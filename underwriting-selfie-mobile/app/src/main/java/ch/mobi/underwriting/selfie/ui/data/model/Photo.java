package ch.mobi.underwriting.selfie.ui.data.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Photo {

	Long id;

	@JsonIgnore
	String dataUrl;

	public Photo(Long id, String dataUrl) {
		this.id = id;
		this.dataUrl = dataUrl;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataUrl() {
		return dataUrl;
	}

	public void setDataUrl(String dataUrl) {
		this.dataUrl = dataUrl;
	}

}
