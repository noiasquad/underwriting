package ch.mobi.underwriting.selfie.ui.task;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

import java.io.IOException;
import java.lang.ref.WeakReference;

import ch.mobi.underwriting.selfie.ui.data.retrofit.PredictionService;
import ch.mobi.underwriting.selfie.ui.data.retrofit.ProxyFactory;
import retrofit2.Response;

public class RemoteServerLivenessCheck extends AsyncTask<Void, Void, Response> {

    private WeakReference<Context> context;

    public RemoteServerLivenessCheck(Context context) {
        this.context = new WeakReference<>(context);
    }

    @Override
    protected Response doInBackground(Void ...params) {
        try {
            PredictionService predictionService = ProxyFactory.create(PredictionService.class);
            return predictionService.ping().execute();
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Response result) {
        if (result != null) {
            if (!result.isSuccessful()) {
                Toast.makeText(context.get(), "The prediction service is not available: " + result.code(), Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(context.get(), "Impossible to reach the prediction service", Toast.LENGTH_LONG).show();
        }
    }
}