package ch.mobi.underwriting.selfie.ui.data.retrofit;

import ch.mobi.underwriting.selfie.ui.data.model.Prediction;
import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface PredictionService {

	String DATA_PARAMETER = "data";
	String PREDICTION_ENDPOINT = "/api/profiling/v1/predictions";
	String ALL_PREDICTION_PATH = "/all";
	String PING_PATH = "/ping";

	@Multipart
	@POST(PREDICTION_ENDPOINT + ALL_PREDICTION_PATH)
	Call<Prediction> upload(@Part MultipartBody.Part faceImage);

	@GET(PREDICTION_ENDPOINT + PING_PATH)
	Call<ResponseBody> ping();
}
