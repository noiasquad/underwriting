package ch.mobi.underwriting.selfie.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import ch.mobi.underwriting.selfie.ui.R;
import ch.mobi.underwriting.selfie.ui.data.model.Prediction;
import ch.mobi.underwriting.selfie.ui.data.model.Profile;

public class CorrectionActivity extends AppCompatActivity {

    private static final String BUNDLE_PREDICTION_ID = "prediction-id";

    private Prediction prediction;
    private Profile profile;

    private int buttonId = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        prediction = (Prediction) intent.getSerializableExtra(BUNDLE_PREDICTION_ID);
        profile = prediction.getProfile();

        setContentView(R.layout.activity_correction);
        Toolbar toolbar = findViewById(R.id.toolbar_main);
        setSupportActionBar(toolbar);

        LinearLayout correctionLayout = findViewById(R.id.content_correction_layout);

        showPrediction(correctionLayout, R.string.lblGender, R.array.gender_labels, prediction.getContent()[0], profile.getGender(), clazz -> profile.setGender(clazz));
        showPrediction(correctionLayout, R.string.lblAge, R.array.age_labels, prediction.getContent()[1], profile.getAge(), clazz -> profile.setAge(clazz));
        showPrediction(correctionLayout, R.string.lblWeight, R.array.weight_labels, prediction.getContent()[2], profile.getWeight(), clazz -> profile.setWeight(clazz));

        FloatingActionButton fab = findViewById(R.id.fab_save);
        fab.setOnClickListener(view -> saveCorrection());
    }

    interface ProfileUpdater {
        void update(int clazz);
    }

    private void showPrediction(LinearLayout predictionLayout,
                                int classNameResId,
                                int labelNamesResId,
                                double[] probabilities,
                                int prediction,
                                ProfileUpdater profileUpdater) {

        String[] labelNames = getResources().getStringArray(labelNamesResId);

        TextView headerText = new TextView(this);
        headerText.setText(getString(classNameResId));
        headerText.setTextSize(1, 24);
        headerText.setTypeface(headerText.getTypeface(), Typeface.BOLD);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 10, 0, 10);

        predictionLayout.addView(headerText, layoutParams);

        RadioGroup predictionGroup = new RadioGroup(this);
        predictionLayout.addView(predictionGroup, new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));

        LinearLayout.LayoutParams toggleLayoutParam = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        toggleLayoutParam.setMargins(0, 0, 0, 10);

        for (int classIdx = 0; classIdx < probabilities.length; classIdx++) {
            RadioButton toggleClass = new RadioButton(this);
            toggleClass.setId(buttonId++);
            toggleClass.setTextSize(18);
            toggleClass.setText(labelNames[classIdx]);

            final int clazz = classIdx;
            toggleClass.setOnClickListener(view -> profileUpdater.update(clazz));

            predictionGroup.addView(toggleClass, toggleLayoutParam);

            if (classIdx == prediction) {
                predictionGroup.check(toggleClass.getId());
            }
        }
    }

    public static Intent makeIntent(Context context, Prediction prediction) {
        Intent intent = new Intent(context, CorrectionActivity.class);
        intent.putExtra(BUNDLE_PREDICTION_ID, prediction);
        return intent;
    }

    private void saveCorrection() {
        Toast.makeText(getApplicationContext(), profile.toString(), Toast.LENGTH_LONG).show();
    }
}
