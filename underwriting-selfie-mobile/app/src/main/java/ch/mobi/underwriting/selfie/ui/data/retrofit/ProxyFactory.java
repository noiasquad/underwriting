package ch.mobi.underwriting.selfie.ui.data.retrofit;

import ch.mobi.underwriting.selfie.ui.util.Constants;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

public class ProxyFactory {

    private static final String BASE_URL = Constants.SERVER_URL;

    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(JacksonConverterFactory.create());

    private static Retrofit retrofit = builder.client(httpClient.build()).build();

    public static <S> S create(Class<S> serviceClass) {
        return retrofit.create(serviceClass);
    }
}
