Selfie underwriting task
========================================================
author: olivier.vondach@mobi.ch
date: May 29, 2018
autosize: true
transition: linear
transition-speed: slow

The Underwriting task
========================================================
It aims to **classify** a physical person into a class of insurance risk.

Question
========================================================
Is it possible to optimize the acquisition of individual data during the underwriting task by using a **predictive approach** based on a self photograph?

Answers
========================================================
incremental: true

Since 2012 popular **Facial recognition and deep learning techniques** support the elaboration of predictive classification models, trained on face representations provided by large social data sets, and make it possible to estimate age, gender and body mass index, with a moderately or very acceptable accuracy.

Several commercial **Cloud-based APIs** have emerged and provide a detection API for person, face, gender, age, and facial landmark: [Sighthound](https://www.sighthound.com/products/cloud), [SkyBiometry](https://skybiometry.com/), [FindFace](https://api.findface.pro/v1/docs/v1/overview.html)

This **Selfie application** illustrates the user experience that an individual could have on web and mobile devices: [here](https://selfie.obya.ch:9000)

Performance numbers
========================================================
incremental: true

The performance of a popular state of the art method (Hassner & Levi) applied on unconstrained and challenging face images ([Adience dataset](https://www.openu.ac.il/home/hassner/Adience/data.html#agegender)):

* Gender classification: over 80% of accuracy with a standard error of around 1%
* Age classification (exact): around 50% of accuracy with a standard error of around 5%
* Age classification (1-off): over 80% of accuracy with a standard error of around 2%

Some commercial Cloud-based solutions fortunately seem to outperform these numbers (91%, 70%  96.2%): [Sighthound](https://www.sighthound.com/technology/age-gender-emotion/benchmarks)

Potential for insurance domain
========================================================
incremental: true

**In my opinion** after this short period studying the question...

The predictive methods for Age, Gender estimation supported by the state of the art are not reliable enough to efficiently support or replace the traditional way of working, considering that a validation step is still necessary to complete a predictive approach.

Nevertheless a it is obvious that it cannot be a definitive conclusion...

Maybe it is possible for an underwriter to already use the potential of these last interesting accuracy numbers and their confidence interval, dimensioning the classes smartly.

Annexes
========================================================
- Link to the [detailed documentation](http://underwriting.obya.ch/selfie-underwriting-task.html) of the underlying study

