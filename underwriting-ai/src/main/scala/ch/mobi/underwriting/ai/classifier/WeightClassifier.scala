package ch.mobi.underwriting.ai.classifier

import java.nio.file.Path

import ch.mobi.ai.domain.{Classifier, Prediction}

import scala.util.{Success, Try}

object WeightClassifier extends Classifier[Path, Int] {

  override def classify(faceImage: Path): Try[Array[Prediction[Int]]] =
    Success(Array(
      Prediction(0, 0.0),
      Prediction(1, 0.1),
      Prediction(2, 0.8),
      Prediction(3, 0.1),
      Prediction(4, 0.0),
      Prediction(5, 0.0)))
}
