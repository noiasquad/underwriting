package ch.mobi.underwriting.domain.service.ai.classifier.gender.model

import org.deeplearning4j.nn.graph.ComputationGraph

class GenderClassifierModel(val height: Int, val width: Int, val channels: Int, val nClasses: Int) {

  private val network = new ComputationGraph(new GenderClassifierNetworkConfig(height, width, channels, nClasses).config)

  def model: ComputationGraph = network
}
