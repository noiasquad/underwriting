package ch.mobi.underwriting.domain.service.ai.classifier.gender.model

import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution
import org.deeplearning4j.nn.conf.inputs.InputType
import org.deeplearning4j.nn.conf.layers._
import org.deeplearning4j.nn.conf.{ComputationGraphConfiguration, NeuralNetConfiguration}
import org.deeplearning4j.nn.weights.WeightInit
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.learning.config.Sgd
import org.nd4j.linalg.schedule.{MapSchedule, ScheduleType}

class GenderClassifierNetworkConfig(val height: Int, val width: Int, val channels: Int, val nClasses: Int) {

  private val seed: Int = 12345

  private val learningRateSchedule: java.util.Map[Integer, java.lang.Double] = new java.util.HashMap[Integer, java.lang.Double]
  learningRateSchedule.put(0, 1e-3)
  learningRateSchedule.put(10000, 1e-4)

  def config: ComputationGraphConfiguration = new NeuralNetConfiguration.Builder()
    .seed(seed)
    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
    .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, learningRateSchedule)))

    .graphBuilder()
    .setInputTypes(InputType.convolutional(height, width, channels))
    .addInputs("input")

    //conv1-relu1
    .addLayer("conv1-relu1", new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(7, 7)
    .padding(0, 0)
    .stride(4, 4)
    .nIn(3)
    .nOut(96)
    .activation(Activation.RELU)
    .build(), "input")

    //pool1
    .addLayer("pool1", new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build(), "conv1-relu1")

    //norm1
    .addLayer("norm1", new LocalResponseNormalization.Builder()
    .n(5)
    .alpha(0.0001)
    .beta(0.75)
    .build(), "pool1")

    //conv2-relu2
    .addLayer("conv2-relu2", new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(5, 5)
    .padding(2, 2)
    .nOut(256)
    .activation(Activation.RELU)
    .build(), "norm1")

    //pool2
    .addLayer("pool2", new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build(), "conv2-relu2")

    //norm2
    .addLayer("norm2", new LocalResponseNormalization.Builder()
    .n(5)
    .alpha(0.0001)
    .beta(0.75)
    .build(), "pool2")

    //conv3-relu3
    .addLayer("conv3-relu3", new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(3, 3)
    .padding(1, 1)
    .nOut(384)
    .activation(Activation.RELU)
    .build(), "norm2")

    //pool5
    .addLayer("pool5", new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build(), "conv3-relu3")

    //fc6-relu6-drop6
    .addLayer("fc6-relu6-drop6", new DenseLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.005))
    .biasInit(1)
    .nOut(512)
    .activation(Activation.RELU)
    .dropOut(0.5)
    .build(), "pool5")

    //fc7-relu7-drop7
    .addLayer("fc7-relu7-drop7", new DenseLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.005))
    .biasInit(1)
    .nOut(512)
    .activation(Activation.RELU)
    .dropOut(0.5)
    .build(), "fc6-relu6-drop6")

    //fc8-prob
    .addLayer("fc8-prob", new OutputLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .nOut(nClasses)
    .activation(Activation.SOFTMAX)
    .build(), "fc7-relu7-drop7")

    .backprop(true)
    .pretrain(false)
    .build()
}
