package ch.mobi.underwriting.domain.service.ai.classifier.age.training

import java.io.File

import ch.mobi.underwriting.domain.service.ai.classifier.age.dataset.AdienceAgeDataSet
import ch.mobi.underwriting.domain.service.ai.classifier.age.model.AgeClassifierModel
import org.apache.commons.io.FilenameUtils
import org.deeplearning4j.earlystopping.listener.EarlyStoppingListener
import org.deeplearning4j.earlystopping.saver.LocalFileModelSaver
import org.deeplearning4j.earlystopping.scorecalc.DataSetLossCalculator
import org.deeplearning4j.earlystopping.termination.{MaxEpochsTerminationCondition, ScoreImprovementEpochTerminationCondition}
import org.deeplearning4j.earlystopping.trainer.EarlyStoppingTrainer
import org.deeplearning4j.earlystopping.{EarlyStoppingConfiguration, EarlyStoppingResult}
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.ui.api.UIServer
import org.deeplearning4j.ui.stats.StatsListener
import org.deeplearning4j.ui.storage.InMemoryStatsStorage
import org.deeplearning4j.util.ModelSerializer
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.slf4j.LoggerFactory

object AgeClassifierTrainer {

  private val log = LoggerFactory.getLogger(getClass.getSimpleName)

  private val TRAINING_BASE_PATH = System.getProperty("training.dir")

  log.info(s"Training base path is $TRAINING_BASE_PATH")

  private val TRAINING_EPOCHS = 20
  private val MAX_EPOCHS_WITHOUT_IMPROVEMENT = 5
  private val LIVE_EVALUATION_FREQUENCY_EPOCHS = 2
  private val VALIDATION_TRAINING_EPOCHS = 5
  private val VALIDATION_LIVE_EVALUATION_FREQUENCY_EPOCHS = 6
  private val VALIDATION_TRAINING_ACCURACY_ACCEPTANCE = 0.2

  def main(args: Array[String]): Unit = {

    val (validationDir, trainingDir, deliveryDir) = configureOutputDirectories(new File(TRAINING_BASE_PATH))

    val model: MultiLayerNetwork = initializeModel

    configureMonitoring(model)

    val (smallTrainingDS, smallTestingDS) = (AdienceAgeDataSet.smallTrainingFolds, AdienceAgeDataSet.smallTestingDSIt)

    val averageAccuracy = crossValidateModel(model, smallTrainingDS, smallTestingDS, validationDir)

    if (averageAccuracy > VALIDATION_TRAINING_ACCURACY_ACCEPTANCE) {
      val (trainingDS, testingDS) = (AdienceAgeDataSet.trainingDSIt, AdienceAgeDataSet.testingDSIt)

      trainModel(model, trainingDS, smallTestingDS, trainingDir, TRAINING_EPOCHS, LIVE_EVALUATION_FREQUENCY_EPOCHS)
      saveModel(model, deliveryDir)
      testModel(model, testingDS)
    }
  }

  private def initializeModel = {
    log.info("Initialization of the model before training...")

    val model: MultiLayerNetwork = new AgeClassifierModel(
      AdienceAgeDataSet.FEATURE_IMAGE_HEIGHT,
      AdienceAgeDataSet.FEATURE_IMAGE_WIDTH,
      AdienceAgeDataSet.FEATURE_IMAGE_DEPTH,
      AdienceAgeDataSet.CLASS_COUNT).model

    model.init()

    log.info("The configured network layers are:")
    for (l <- model.getLayers) {
      log.info(s"${l.conf.getLayer.getLayerName}\t${l.numParams}")
    }
    model
  }

  private def crossValidateModel(model: MultiLayerNetwork,
                                 trainingDS: List[File],
                                 testingDSIt: DataSetIterator,
                                 outputDir: File): Double = {

    log.info(s"${trainingDS.size}-fold cross-validation of the model...")

    def train(head: List[File], tail: List[File], evalList: List[Double]): List[Double] = {
      tail match {
        case testingFold :: trainingFolds =>
          val ts: List[File] = head ++ trainingFolds

          log.info(s"The next testing fold is ${testingFold.getName} and next training folds are " + ts)

          trainModel(model,
            AdienceAgeDataSet.dataSetIt(ts),
            testingDSIt,
            trainingDir(s"fold-${testingFold.getName}"),
            VALIDATION_TRAINING_EPOCHS,
            VALIDATION_LIVE_EVALUATION_FREQUENCY_EPOCHS)

          val evaluation = testModel(model, AdienceAgeDataSet.dataSetIt(List(testingFold)))

          train(testingFold :: head, trainingFolds, evaluation :: evalList)

        case _ => evalList
      }
    }

    def trainingDir(foldName: String) = createDir(outputDir, foldName)

    val foldEvaluations = train(List(), trainingDS, List())
    val averageEvaluation = foldEvaluations.sum / foldEvaluations.size

    log.info(s"The result of ${foldEvaluations.size}-fold cross-validation of the model is $averageEvaluation")

    averageEvaluation
  }

  private def trainModel(model: MultiLayerNetwork,
                         trainingDSIt: DataSetIterator,
                         earlyStoppingTestingDSIt: DataSetIterator,
                         outputDir: File,
                         epochs: Int,
                         earlyValidationEpochs: Int): MultiLayerNetwork = {

    log.info("Training of the model...")

    val esConfig: EarlyStoppingConfiguration[MultiLayerNetwork] =
      new EarlyStoppingConfiguration.Builder[MultiLayerNetwork]
        .epochTerminationConditions(
          new MaxEpochsTerminationCondition(epochs),
          new ScoreImprovementEpochTerminationCondition(MAX_EPOCHS_WITHOUT_IMPROVEMENT))
        .scoreCalculator(new DataSetLossCalculator(earlyStoppingTestingDSIt, true))
        .evaluateEveryNEpochs(earlyValidationEpochs)
        .modelSaver(new LocalFileModelSaver(outputDir.getAbsolutePath))
        .saveLastModel(false)
        .build()

    val listener = new EarlyStoppingListener[MultiLayerNetwork] {
      override def onStart(esConfig: EarlyStoppingConfiguration[MultiLayerNetwork], net: MultiLayerNetwork): Unit = {}

      override def onEpoch(epochNum: Int, score: Double, esConfig: EarlyStoppingConfiguration[MultiLayerNetwork], net: MultiLayerNetwork): Unit = {
        log.info(s"The training score at epoch $epochNum is $score")
      }

      override def onCompletion(esResult: EarlyStoppingResult[MultiLayerNetwork]): Unit = {
        log.info("The training synthetic result is\n" + esResult)
        log.info("The summarized scoring after each training epoch is " + esResult.getScoreVsEpoch)
      }
    }

    model.init()
    new EarlyStoppingTrainer(esConfig, model, trainingDSIt, listener).fit().getBestModel
  }

  private def testModel(model: MultiLayerNetwork, testingSet: DataSetIterator): Double = {
    log.info("Testing of the trained model...")

    val evaluation = model.evaluate(testingSet)

    log.info("The evaluation result of the model is\n" + evaluation.stats)
    log.info("The confusion matrix is\n" + evaluation.confusionToString())

    evaluation.accuracy()
  }

  private def saveModel(model: MultiLayerNetwork, outputDir: File) = {
    log.info("Saving the trained model...")

    ModelSerializer.writeModel(model,
      FilenameUtils.concat(outputDir.getAbsolutePath, "age_prediction_model.zip"), true)
  }

  private def configureMonitoring(model: MultiLayerNetwork) = {
    log.info("Configuration of monitoring...")

    val uiServer = UIServer.getInstance
    val statsStorage = new InMemoryStatsStorage

    model.setListeners(
      new StatsListener(statsStorage, 10),
      new ScoreIterationListener(100))

    uiServer.attach(statsStorage)
  }

  private def configureOutputDirectories(baseDir: File) = (
    createDir(baseDir, "validation"),
    createDir(baseDir, "training"),
    createDir(baseDir, "delivery")
  )

  private def createDir(baseDir: File, name: String): File = {
    log.info(s"Creation of $name output directory...")
    val dir = new File(FilenameUtils.concat(baseDir.getAbsolutePath, name))
    dir.mkdir
    dir
  }
}
