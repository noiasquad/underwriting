package ch.mobi.underwriting.domain.service.ai.classifier.age.model

import org.deeplearning4j.nn.multilayer.MultiLayerNetwork

class AgeClassifierModel(val height: Int, val width: Int, val channels: Int, val nClasses: Int) {

  def model: MultiLayerNetwork = new MultiLayerNetwork(
    new AgeClassifierNetworkConfig(height, width, channels, nClasses).config)
}
