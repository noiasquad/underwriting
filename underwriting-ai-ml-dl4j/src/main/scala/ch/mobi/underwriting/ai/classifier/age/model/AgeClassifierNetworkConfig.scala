package ch.mobi.underwriting.domain.service.ai.classifier.age.model

import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.distribution.GaussianDistribution
import org.deeplearning4j.nn.conf.inputs.InputType
import org.deeplearning4j.nn.conf.layers._
import org.deeplearning4j.nn.conf.{MultiLayerConfiguration, NeuralNetConfiguration}
import org.deeplearning4j.nn.weights.WeightInit
import org.nd4j.linalg.activations.Activation
import org.nd4j.linalg.learning.config.Sgd
import org.nd4j.linalg.schedule.{MapSchedule, ScheduleType}

class AgeClassifierNetworkConfig(val height: Int, val width: Int, val channels: Int, val nClasses: Int) {

  private val seed: Int = 12345

  private val learningRateSchedule: java.util.Map[Integer, java.lang.Double] = new java.util.HashMap[Integer, java.lang.Double]
  learningRateSchedule.put(0, 1e-3)
  learningRateSchedule.put(10000, 1e-4)

  def config: MultiLayerConfiguration = new NeuralNetConfiguration.Builder()
    .seed(seed)
    .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
    .updater(new Sgd(new MapSchedule(ScheduleType.ITERATION, learningRateSchedule)))

    .list()
    .setInputType(InputType.convolutional(height, width, channels))

    //conv1-relu1
    .layer(0, new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(7, 7)
    .padding(0, 0)
    .stride(4, 4)
    .nIn(3)
    .nOut(96)
    .activation(Activation.RELU)
    .build())

    //pool1
    .layer(1, new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build())

    //norm1
    .layer(2, new LocalResponseNormalization.Builder()
    .n(5)
    .alpha(0.0001)
    .beta(0.75)
    .build())

    //conv2-relu2
    .layer(3, new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(5, 5)
    .padding(2, 2)
    .nOut(256)
    .activation(Activation.RELU)
    .build())

    //pool2
    .layer(4, new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build())

    //norm2
    .layer(5, new LocalResponseNormalization.Builder()
    .n(5)
    .alpha(0.0001)
    .beta(0.75)
    .build())

    //conv3-relu3
    .layer(6, new ConvolutionLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .kernelSize(3, 3)
    .padding(1, 1)
    .nOut(384)
    .activation(Activation.RELU)
    .build())

    //pool5
    .layer(7, new SubsamplingLayer.Builder(PoolingType.MAX)
    .kernelSize(3, 3)
    .stride(2, 2)
    .padding(0, 0)
    .build())

    //fc6-relu6-drop6
    .layer(8, new DenseLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.005))
    .biasInit(1)
    .nOut(512)
    .activation(Activation.RELU)
    .dropOut(0.5)
    .build())

    //fc7-relu7-drop7
    .layer(9, new DenseLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.005))
    .biasInit(1)
    .nOut(512)
    .activation(Activation.RELU)
    .dropOut(0.5)
    .build())

    //fc8-prob
    .layer(10, new OutputLayer.Builder()
    .weightInit(WeightInit.DISTRIBUTION).dist(new GaussianDistribution(0, 0.01))
    .biasInit(0)
    .nOut(nClasses)
    .activation(Activation.SOFTMAX)
    .build())

    .backprop(true)
    .pretrain(false)
    .build()
}
