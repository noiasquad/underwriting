package ch.mobi.underwriting.domain.service.ai.classifier.age.dataset

import java.io.File

import ch.mobi.underwriting.domain.service.ai.classifier.adience.AdienceDataSetIteratorFactory
import org.datavec.image.transform.{CropImageTransform, MultiImageTransform, ResizeImageTransform}
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator
import org.slf4j.LoggerFactory

/**
  * Each image file is supposed to be stored in a subdirectory which name is the associated supervised label
  * Every image file is re-sized to 256x256 before being cropped
  */
object AdienceAgeDataSet {

  private val log = LoggerFactory.getLogger(getClass.getSimpleName)

  private val DATA_BASE_PATH = System.getProperty("data.dir")

  log.info(s"Data base path is $DATA_BASE_PATH")

  val FEATURE_ALIGNED_IMAGE_HEIGHT = 256
  val FEATURE_ALIGNED_IMAGE_WIDTH = 256
  val FEATURE_IMAGE_CROP = 15
  val FEATURE_IMAGE_HEIGHT: Int = FEATURE_ALIGNED_IMAGE_HEIGHT - 2 * FEATURE_IMAGE_CROP
  val FEATURE_IMAGE_WIDTH: Int = FEATURE_ALIGNED_IMAGE_WIDTH - 2 * FEATURE_IMAGE_CROP
  val FEATURE_IMAGE_DEPTH = 3
  val CLASS_COUNT = 8
  val BATCH_SIZE = 10

  private val beforeReadTransform = new MultiImageTransform(
    new ResizeImageTransform(FEATURE_ALIGNED_IMAGE_WIDTH, FEATURE_ALIGNED_IMAGE_HEIGHT),
    new CropImageTransform(FEATURE_IMAGE_CROP))

  def dataSetIt(folds: List[File]): DataSetIterator = AdienceDataSetIteratorFactory.create(
    folds,
    FEATURE_IMAGE_HEIGHT,
    FEATURE_IMAGE_WIDTH,
    FEATURE_IMAGE_DEPTH,
    BATCH_SIZE,
    CLASS_COUNT,
    beforeReadTransform
  )

  def trainingFolds = List(
    new File(s"$DATA_BASE_PATH/age_train_0"),
    new File(s"$DATA_BASE_PATH/age_train_1"),
    new File(s"$DATA_BASE_PATH/age_train_2"),
    new File(s"$DATA_BASE_PATH/age_train_3"),
    new File(s"$DATA_BASE_PATH/age_train_4"))

  def trainingDSIt: DataSetIterator = AdienceDataSetIteratorFactory.create(
    trainingFolds,
    FEATURE_IMAGE_HEIGHT,
    FEATURE_IMAGE_WIDTH,
    FEATURE_IMAGE_DEPTH,
    BATCH_SIZE,
    CLASS_COUNT,
    beforeReadTransform
  )

  def smallTrainingFolds = List(
    new File(s"$DATA_BASE_PATH/age_train_subset_0"),
    new File(s"$DATA_BASE_PATH/age_train_subset_1"),
    new File(s"$DATA_BASE_PATH/age_train_subset_2"),
    new File(s"$DATA_BASE_PATH/age_train_subset_3"),
    new File(s"$DATA_BASE_PATH/age_train_subset_4"))

  def smallTrainingDSIt: DataSetIterator = AdienceDataSetIteratorFactory.create(
    smallTrainingFolds,
    FEATURE_IMAGE_HEIGHT,
    FEATURE_IMAGE_WIDTH,
    FEATURE_IMAGE_DEPTH,
    BATCH_SIZE,
    CLASS_COUNT,
    beforeReadTransform
  )

  def testingFolds = List(
    new File(s"$DATA_BASE_PATH/age_testing_0"),
    new File(s"$DATA_BASE_PATH/age_testing_1"),
    new File(s"$DATA_BASE_PATH/age_testing_2"),
    new File(s"$DATA_BASE_PATH/age_testing_3"),
    new File(s"$DATA_BASE_PATH/age_testing_4"))

  def testingDSIt: DataSetIterator = AdienceDataSetIteratorFactory.create(
    testingFolds,
    FEATURE_IMAGE_HEIGHT,
    FEATURE_IMAGE_WIDTH,
    FEATURE_IMAGE_DEPTH,
    BATCH_SIZE,
    CLASS_COUNT,
    beforeReadTransform
  )

  def smallTestingFolds = List(
    new File(s"$DATA_BASE_PATH/age_val_0"),
    new File(s"$DATA_BASE_PATH/age_val_1"),
    new File(s"$DATA_BASE_PATH/age_val_2"),
    new File(s"$DATA_BASE_PATH/age_val_3"),
    new File(s"$DATA_BASE_PATH/age_val_4"))

  def smallTestingDSIt: DataSetIterator = AdienceDataSetIteratorFactory.create(
    smallTestingFolds,
    FEATURE_IMAGE_HEIGHT,
    FEATURE_IMAGE_WIDTH,
    FEATURE_IMAGE_DEPTH,
    BATCH_SIZE,
    CLASS_COUNT,
    beforeReadTransform
  )
}
