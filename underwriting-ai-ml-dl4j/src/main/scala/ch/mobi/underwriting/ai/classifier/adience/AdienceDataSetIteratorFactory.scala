package ch.mobi.underwriting.domain.service.ai.classifier.adience

import java.io.File
import java.net.URI

import org.datavec.api.io.labels.ParentPathLabelGenerator
import org.datavec.api.split.{CollectionInputSplit, FileSplit, InputSplit}
import org.datavec.image.recordreader.ImageRecordReader
import org.datavec.image.transform.ImageTransform
import org.deeplearning4j.datasets.datavec.RecordReaderDataSetIterator
import org.nd4j.linalg.dataset.api.preprocessor.{DataNormalization, ImagePreProcessingScaler}
import org.slf4j.LoggerFactory

import scala.collection.mutable.ArrayBuffer

/**
  * Builds a complex RecordReader instance from multiple input fold directories
  * Individual RecordReader instance are cached for reuse, it is useful during k-fold validation
  *
  * Each image file is supposed to be stored in a subdirectory which name is the associated supervised label
  * Every image file is re-sized to 256x256 before being cropped and finally rescaled to (0,1)
  */
object AdienceDataSetIteratorFactory {

  private val log = LoggerFactory.getLogger(getClass.getSimpleName)

  private val cache = collection.mutable.Map[String, InputSplit]()

  private val preprocessor: DataNormalization = new ImagePreProcessingScaler

  def create(folds: List[File], height: Int, width: Int, depth: Int, batchSize: Int, labelCount: Int, transform: ImageTransform): RecordReaderDataSetIterator = {
    log.info(s"Create multi dataSet iterator on ${folds.size} folds")

    val reader = new ImageRecordReader(height, width, depth, new ParentPathLabelGenerator)

    reader.initialize(toInputSplit(folds), transform)

    val iterator = new RecordReaderDataSetIterator(reader, batchSize, 1, labelCount)
    iterator.setPreProcessor(preprocessor)
    iterator
  }

  private def toInputSplit(folds: List[File]): InputSplit = {
    val uriBuffer = new ArrayBuffer[URI]

    folds
      .map(fold => cache.getOrElseUpdate(fold.getName, new FileSplit(fold)))
      .map(fs => fs.locations)
      .foreach(uris => uriBuffer ++= uris)

    val uris = util.Random.shuffle(uriBuffer).toArray[URI]
    new CollectionInputSplit(uris)
  }
}
