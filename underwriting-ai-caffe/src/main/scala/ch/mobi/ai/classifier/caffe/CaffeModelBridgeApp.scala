/**
  * https://github.com/bytedeco/sample-projects/tree/master/caffe-sample-classifier/src/main/java/org/bytedeco/javacpp/caffe_example
  */
package ch.mobi.ai.classifier.caffe

import java.io.{BufferedReader, FileReader}
import java.nio.file.Paths
import java.util.stream.Collectors

import scala.util.{Failure, Success}

class CaffeModelBridgeApp {

  def main(args: Array[String]) {
    if (args.length != 5) {
      System.err.println("Usage: java -jar jarfile.jar deploy.prototxt network.caffemodel mean.binaryproto labels.txt image.jpg");
      System.exit(1)
    }

    val files = args.map((path: String) => Paths.get(path))

    val (modelFile, trainedFile, meanFile, labelFile, imageFile) = (files(0), files(1), files(2), files(3), files(4))

    val labels = new BufferedReader(new FileReader(labelFile.toFile)).lines().collect(Collectors.toList())

    CaffeModel.create(modelFile, trainedFile, meanFile)
      .map(CaffeClassifier(_))
      .flatMap(_.classify(imageFile)) match {
      case Success(p) => p.foreach(prediction =>
        println(s"${labels.get(prediction.label)}: ${prediction.confidence}"))
      case Failure(e) => println(e.getMessage)
    }
  }
}
