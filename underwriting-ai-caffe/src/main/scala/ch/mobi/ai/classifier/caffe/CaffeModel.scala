package ch.mobi.ai.classifier.caffe

import java.nio.file._

import ch.mobi.ai.domain.{Model, Prediction}

import scala.util.{Success, Try}

class CaffeModel(val caffe: CaffeModelBridge) extends Model[Path, Int] {

  override def model: Path => Try[Array[Prediction[Int]]] =
    faceImage => Success(
      caffe
        .predict(CaffeModelBridge.loadImage(faceImage.toString))
        .zipWithIndex.map { case (confidence, index) => Prediction(index, confidence) })

  override def init: Unit = {}
}

object CaffeModel {

  def create(modelDefinition: Path, pretrainedModel: Path, meanImage: Path): Try[CaffeModel] =
    Success(CaffeModel(CaffeModelBridge.create(
      modelDefinition.toString,
      pretrainedModel.toString,
      meanImage.toString)))
}
