package ch.mobi.ai.classifier.caffe

import java.nio.file.Path

import ch.mobi.ai.domain.{Classifier, Prediction}
import org.bytedeco.javacpp.caffe.Caffe

import scala.util.{Failure, Try}

case class CaffeClassifier(caffe: CaffeModel, mode: Int = Caffe.CPU) extends Classifier[Path, Int] {

  override def classify(faceImage: Path): Try[Array[Prediction[Int]]] = {
    Caffe.set_mode(mode)
    caffe.model(faceImage)
  }
}

object CaffeClassifier {
  def gpu(caffe: CaffeModel) = CaffeClassifier(caffe, Caffe.GPU)
}

