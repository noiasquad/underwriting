package ch.mobi.underwriting.ai.classifier

import java.nio.file.Path

import ch.mobi.ai.classifier.caffe.CaffeModel.create
import ch.mobi.ai.classifier.caffe.{CaffeClassifier, CaffeModel}
import ch.mobi.ai.domain.{Classifier, Prediction}

import scala.util.{Failure, Success, Try}

class AgeClassifier(val rootPath: Path, val gpuMode: Boolean = false) extends Classifier[Path, Int] {

  private val caffe: CaffeClassifier = model(rootPath) match {
    case Success(model) => if (gpuMode) CaffeClassifier(model) else CaffeClassifier.gpu(model)
    case Failure(e) => throw e
  }

  override def classify(faceImage: Path): Try[Array[Prediction[Int]]] = caffe.classify(faceImage)

  private def model(rootPath: Path): Try[CaffeModel] = {
    val defaultAgeModelDefinitionPath = "age/age_deploy.prototxt"
    val defaultAgeTrainedModelPath = "age/age_net.caffemodel"
    val defaultMeanImagePath = "mean/mean.binaryproto"
    create(
      rootPath.resolve(defaultAgeModelDefinitionPath),
      rootPath.resolve(defaultAgeTrainedModelPath),
      rootPath.resolve(defaultMeanImagePath))
  }
}
