package ch.mobi.underwriting.ai.classifier

import java.nio.file.Path

import ch.mobi.ai.classifier.caffe.CaffeModel.create
import ch.mobi.ai.classifier.caffe.{CaffeClassifier, CaffeModel}
import ch.mobi.ai.domain.{Classifier, Prediction}

class GenderClassifier(val rootPath: Path, val gpuMode: Boolean = false) extends Classifier[Path, Int] {

  private val caffe = model(rootPath) match {
    case Right(model) => if (gpuMode) CaffeClassifier(model) else CaffeClassifier.gpu(model)
    case Left(e) => throw e
  }

  override def classify(faceImage: Path): Either[Exception, Array[Prediction[Int]]] = caffe.classify(faceImage)

  private def model(rootPath: Path): Either[Exception, CaffeModel] = {
    val defaultGenderModelDefinitionPath = "gender/gender_deploy.prototxt"
    val defaultGenderTrainedModelPath = "gender/gender_net.caffemodel"
    val defaultMeanImagePath = "mean/mean.binaryproto"
    create(
      rootPath.resolve(defaultGenderModelDefinitionPath),
      rootPath.resolve(defaultGenderTrainedModelPath),
      rootPath.resolve(defaultMeanImagePath))
  }
}
