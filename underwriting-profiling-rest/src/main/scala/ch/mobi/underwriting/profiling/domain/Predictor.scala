package ch.mobi.underwriting.profiling.domain

import java.nio.file.{Path, Paths}

import ch.mobi.ai.domain.Classifier
import ch.mobi.underwriting.ai.classifier.{AgeClassifier, GenderClassifier, WeightClassifier}

import scala.util.Try

class Predictor(name: String, classifier: Classifier[Path, Int]) {
  def predict(faceImage: Path): Try[Map[String, Any]] = classifier.classify(faceImage).map(p => Map(name -> p))
}

object Gender extends Predictor("gender", new GenderClassifier(Paths.get(""))) {}

object Age extends Predictor("age", new AgeClassifier(Paths.get(""))) {}

object Weight extends Predictor("weight", WeightClassifier.classify) {}
