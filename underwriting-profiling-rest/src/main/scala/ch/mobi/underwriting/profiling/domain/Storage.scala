package ch.mobi.underwriting.profiling.domain

import java.io.{File, InputStream}
import java.nio.file.{Files, StandardCopyOption}
import java.util.UUID

import scala.util.Success

object Storage {

  val uploadDir = ""

  def store(fileName: String, content: InputStream, id: UUID) = {
    val uri = new File(s"${uploadDir}/${compose(fileName, id)}").toPath
    Success(Files.copy(content, uri, StandardCopyOption.REPLACE_EXISTING)).map(_ => uri)
  }

  private def compose(originalFilename: String, id: UUID) = {
    val index = originalFilename.indexOf('.')
    id + (if (index > 0) originalFilename.substring(index) else "")
  }
}
