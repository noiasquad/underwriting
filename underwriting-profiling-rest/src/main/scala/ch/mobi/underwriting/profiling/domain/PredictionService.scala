package ch.mobi.underwriting.profiling.domain

import java.util.UUID

import org.springframework.web.multipart.MultipartFile

import scala.util.{Failure, Success, Try}

class PredictionService {

  def predict(file: MultipartFile, predictors: Array[Predictor]): Try[Map[String, Any]] = {
    val id = UUID.randomUUID
    Storage.store(file.getOriginalFilename, file.getInputStream, id)
      .flatMap(path => predictors
        .map(_.predict(path))
        .reduce((a, b) => merge(a, b, _ ++ _)))
      .map(_ + ("id" -> id))
  }

  private def merge[T](a: Try[T], b: Try[T], f: (T, T) => T): Try[T] = {
    (a, b) match {
      case (Success(aa), Success(bb)) => Success(f(aa, bb))
      case (Failure(_), _) => a
      case (_, Failure(_)) => b
    }
  }
}
