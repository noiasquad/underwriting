package ch.mobi.underwriting.profiling.rest.endpoint;

import ch.mobi.underwriting.profiling.domain.*;
import ch.mobi.underwriting.profiling.domain.PredictionService;
import ch.mobi.underwriting.profiling.rest.dto.CorrectionDTO;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

import static org.springframework.http.ResponseEntity.ok;
import static org.springframework.http.ResponseEntity.status;

@Slf4j
@CrossOrigin
@RestController
@RequestMapping(path = "/api/profiling/v1/predictions")
@FieldDefaults(level = AccessLevel.PRIVATE)
public class PredictionEndpoint {

    PredictionService predictionService = new PredictionService();

    @PostMapping(value = "/gender", consumes = "multipart/form-data")
    public ResponseEntity gender(@RequestParam(value = "data") MultipartFile file) {
        return ok(predictionService.predict(file, new Predictor[]{}).getOrElse(null));
    }

    @PostMapping(value = "/age", consumes = "multipart/form-data")
    public ResponseEntity age(@RequestParam(value = "data") MultipartFile file) {
        return ok(predictionService.predict(file, new Predictor[]{}).getOrElse(null));
    }

    @PostMapping(value = "/weight", consumes = "multipart/form-data")
    public ResponseEntity weight(@RequestParam(value = "data") MultipartFile file) {
        return ok(predictionService.predict(file, new Predictor[]{}).getOrElse(null));
    }

    @PostMapping(value = "/all", consumes = "multipart/form-data")
    public ResponseEntity profile(@RequestParam(value = "data") MultipartFile file) {
        return ok(predictionService.predict(file, new Predictor[]{}).getOrElse(null));
    }

    @PostMapping(value = "/", consumes = "multipart/form-data")
    public ResponseEntity profile(
            @RequestParam(value = "data") MultipartFile file,
            @RequestParam(value = "attribute[]") String[] attributes) {
        return ok().build();
    }

    @PutMapping("/{id}/correction")
    public ResponseEntity train(@PathVariable("id") UUID predictionId, CorrectionDTO correction) {
        return ok(predictionId);
    }

    @GetMapping("/ping")
    public ResponseEntity ping() {
        return status(HttpStatus.OK).build();
    }
}
