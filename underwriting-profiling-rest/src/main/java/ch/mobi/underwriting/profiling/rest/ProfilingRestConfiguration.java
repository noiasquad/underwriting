package ch.mobi.underwriting.profiling.rest;

import ch.mobi.underwriting.profiling.domain.PredictionService;
import ch.mobi.underwriting.profiling.domain.service.PredictionService;
import ch.mobi.underwriting.profiling.domain.service.StorageService;
import ch.mobi.underwriting.profiling.rest.connector.AdditionalHttpConnectorConfiguration;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(AdditionalHttpConnectorConfiguration.class)
@Configuration
public class ProfilingRestConfiguration {

    @Bean
    public StorageService storageService(@Value("${upload.dir}") String storageDir) {
        return new StorageService(storageDir);
    }

    @Bean
    public PredictionService predictionService() {
        return new PredictionService();
    }
}
