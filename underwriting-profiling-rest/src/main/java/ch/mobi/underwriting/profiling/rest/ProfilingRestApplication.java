package ch.mobi.underwriting.profiling.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProfilingRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProfilingRestApplication.class, args);
    }
}
