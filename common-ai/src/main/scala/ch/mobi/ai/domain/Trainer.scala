package ch.mobi.ai.domain

trait Trainer[F, L, E] {

  /**
    * Training of a model with a given data set
    * @param model
    * @param trainingSet
    * @return a new trained model
    */
  def train(model: Model[F, L], trainingSet: List[Fold]): Model[F, L]

  /**
    * Evaluation of a trained model against a set of supposed unknown data
    * @param model
    * @param testingSet
    * @return an evaluation that includes accuracy
    */
  def evaluate(model: Model[F, L], testingSet: List[Fold]): E

  /**
    * k-fold cross-validation of a model on a set of folds
    * @param model
    * @param trainingSet
    * @return a list of evaluations including the accuracy of each in-between trained model
    */
  def validate(model: Model[F, L], trainingSet: List[Fold]): List[E] = {

    def loop(head: List[Fold], tail: List[Fold], eval: List[E]): List[E] = {
      tail match {
        case testingFold :: trainingFolds =>
          model.init
          val trainedModel = train(model, trainingFolds)
          val evaluation = evaluate(trainedModel, List(testingFold))
          loop(testingFold :: head, trainingFolds, evaluation :: eval)

        case _ => eval
      }
    }

    loop(List(), trainingSet, List())
  }
}

/*
  def test(model: Model[F, L],
           testingSet: List[Fold],
           toDataSetIt: List[Fold] => DataSetIterator[F, L],
           evaluator: Evaluator[L, E]): E = {

    evaluator.init
    val dataSetIt = toDataSetIt(testingSet)
    while (dataSetIt.nasNext) {
      val dataSet = dataSetIt.next
      val labels = model.output(dataSet.features)
      evaluator.eval(dataSet.labels, labels)
    }
    evaluator.summarize
  }
}
*/