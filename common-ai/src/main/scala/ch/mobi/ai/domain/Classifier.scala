package ch.mobi.ai.domain

import scala.util.Try

trait Classifier[F, L] {
  def classify(features: F): Try[Array[Prediction[L]]]
}

case class Prediction[L](label: L, confidence: Double)