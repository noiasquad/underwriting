package ch.mobi.ai.domain

trait Evaluator[L, E] {
  def init

  def eval(given: L, expected: L): E

  def summarize: E
}
