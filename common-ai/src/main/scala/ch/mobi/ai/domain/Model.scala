package ch.mobi.ai.domain

import scala.util.Try

trait Model[F, L] {

  /**
    * @return a function makes a prediction from a set of features of type F
    */
  def model: F => Try[Array[Prediction[L]]]

  def init
}
